package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Position{

	private int MaxKapazitaet;
	private String typ;
	private String antrieb;
	private int winkel;// Attribute
	
	// Methoden
 public Raumschiff() {
	 this.antrieb = antrieb;
	 this.MaxKapazitaet = MaxKapazitaet;

	 this.typ = typ;
	 this.winkel = winkel;
 }


	public int getMaxKapazitaet() {
		return MaxKapazitaet;
	}

	public void setMaxKapazitaet(int maxKapazitaet) {
		MaxKapazitaet = maxKapazitaet;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},
				
		};
		return raumschiffShape;
	}

}
